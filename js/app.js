'use strict';

angular.module('tca2014', ['ngCookies', 'angular-jwplayer']).config(["$routeProvider", '$compileProvider', function($routeProvider, $compileProvider) {
	
		 $routeProvider
		.when('/', {templateUrl: 'views/homePage.html'})
		.when('/:network/:showName', {templateUrl: 'views/showPage.html'})
		.otherwise({redirectTo: '/', templateUrl: 'views/homePage.html'});
	}]);



    function MainCtrl($scope, $location)
    {

        /*
        $scope.on('$viewContentLoaded', function(){
            $("body").css('backgroundImage', 'url(../images/landing_page_background.png)');
        });
        */



        $scope.loadShow = function(route){
            $location.path(route);
            //console.log("route = " + route)
            if(route == "/"){
               //$("body").css('backgroundImage', 'url(images/landing_page_background.png)');

             // $("body").addClass("bodyBackground");
            } else{
               // $("body").removeClass("bodyBackground")
            }
        }


    }


    function showPageCtrl($scope, $routeParams, $http, $window, $cookies, $rootScope){

        //console.log("showPageCtrl")

        $scope.videoUsername = "viacom";
        $scope.videoPassword =  "tcasummer14";

        var videoStatus = $cookies.videoStatus;

        //console.log("$cookies.videoStatus = " + $cookies.videoStatus);

        if($cookies.videoStatus === "loggedIn"){

            $('#vid_overlay').css('visibility','hidden');
        }else{

           $('#vid_overlay').css('visibility','visible');
        }



        var showNameOnDisplay;
        var JSONFileToLoad;
       // $("body").css('backgroundImage', 'none');

        if($routeParams.network == "mtv"){
            $scope.networkLogo = "images/mtv_logo.png";
        }else{
            $scope.networkLogo = "images/vh1_logo.png";
        }

        switch($routeParams.showName)
        {
            case "FindingCarter": showNameOnDisplay = "Finding Carter";
                JSONFileToLoad = "carter.json";

                break;

            case "Happyland": showNameOnDisplay = "Happyland";
                JSONFileToLoad = "happyland.json";
                break;


            case "VirginTerritory": showNameOnDisplay = "Virgin Territory";
                JSONFileToLoad = "virgin.json";
                break;


            case "CandidlyNicole": showNameOnDisplay = "Candidly Nicole";
                JSONFileToLoad = "nicole.json";
                break;


            case "LeAnn&Eddie": showNameOnDisplay = "LeAnn & Eddie";
                JSONFileToLoad = "eddie.json";
                break;




        }


        $http.get('json/'+JSONFileToLoad).success(function(data){
            //console.log(data);
            //console.log(data[0].images);
            //console.log(data[1].videos);
            //console.log(data[2].press);
            $scope.images = data[0].images;

            //console.log("data[1].videos.length = " + data[1].videos.length);
            if(data[1].videos.length == 0 ){
                $('#videoContainer').css('display','none');
            }else{
                $scope.videos = data[1].videos;
                $scope.videoPoster = data[1].videos[0].poster;
                $scope.mp4Video = data[1].videos[0].video;
                $scope.videoCaption =   data[1].videos[0].caption;
                //console.log($scope.videoPoster)

                $scope.videoOptions = {file: $scope.mp4Video, image:$scope.videoPoster, width:"120%", aspectratio:"16:9",   logo: {hide: "true"}};


                $scope.VideoData =  {file: $scope.mp4Video, image:$scope.videoPoster, width:"120%", aspectratio:"16:9",   logo: {hide: "true"}};

            }

            $scope.releases = data[2].press;
            $scope.twitterMsg =  data[0].images[0].twitter;

            $scope.currentImageNum = 0;
            $('.carousel').carousel('pause');
            $("#downloadGalleryButton").attr({target: '_blank', href  : $scope.images[0].download });
            $('#carouselCaption').html("<p>" + $scope.images[0].caption + "</p>" + "<p>" + $scope.images[0].credit + "</p>");

            //$("#vid_overlay").on("click", function(){});



        });
        $scope.showName = showNameOnDisplay;



        $scope.checkCurrentSlide = function(){
            $('.carousel').on('slid.bs.carousel', function () {

                // This variable contains all kinds of data and methods related to the carousel
                var carouselData = $(this).data('bs.carousel');
                var currentIndex = carouselData.getActiveIndex();
                var total = carouselData.$items.length;

                // Create the text we want to display.
                // We increment the index because humans don't count like machines
                //var text = (currentIndex + 1) + " of " + total;

                 //console.log("Current slide = " + currentIndex);

                $scope.currentImageNum = currentIndex;

                $('#carouselCaption').html("<p>" + $scope.images[currentIndex].caption + "</p>" + "<p>" + $scope.images[currentIndex].credit + "</p>");
            });
        }

        $scope.getPrevImage = function(){
            //console.log("getPrevImage")
            $('.carousel').carousel('prev');
            $scope.checkCurrentSlide();
        }


        $scope.getNextImage = function(){
            //console.log("getNextImage")
            $('.carousel').carousel('next');
            $scope.checkCurrentSlide();
        }



        $scope.getSlider = function()
        {

            $("#cbp-spmenu-s2").toggleClass('cbp-spmenu-open');
        }


        $scope.downloadImage = function (){

            //console.log("$scope.images["+$scope.currentImageNum+"].download = " + $scope.images[$scope.currentImageNum].download);
            //console.log(this);
            $scope.galleryDownload =  $scope.images[$scope.currentImageNum].download
            $("#downloadGalleryButton").attr({target: '_blank', href  : $scope.galleryDownload });
        };


        $scope.checkVideoCreds = function(user){

            if(user.userName == $scope.videoUsername && user.password == $scope.videoPassword)
            {
                //console.log("$cookies.videoStatus = " + $cookies.videoStatus);
                //console.log(user.password);
               // console.log("this works");
                $cookies.videoStatus =   "loggedIn";

                $('#vid_overlay').css('visibility','hidden');
                //console.log("$cookies.videoStatus = " + $cookies.videoStatus);
            }
        }

        $scope.shareWithFB = function(){
           //console.log("sharewithFB")
            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=562213083887048&version=v2.0";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        }


    };